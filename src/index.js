import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './component/App';
import { Router, Switch, Route } from "react-router-dom";
import Connexion from "./component/connexion";
import NotFound from './component/NotFound';
import createBrowserHistory from "history/createBrowserHistory";
import "./style/css/bootstrap.min.css";

const Root = () => {
    const history = createBrowserHistory();
    return(
        <Router history={history} >
            <Switch>
                <Route exact path="/" component={Connexion} />
                <Route path="/box/:pseudo" component={App} />
                <Route component={NotFound} />
            </Switch>
        </Router>
    )
}

render(
    <Root />,
    document.getElementById("root")
);