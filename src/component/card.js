import React from "react";
import PropTypes from "prop-types";

class Card extends React.Component {

    ingredients = this.props.details.ingredients
                       .split(',')
                       .map((item, key) => <li key={key} >{item}</li>)
                       
    instructions = this.props.details.instructions
                   .split('\n')
                   .map((item, key) => <li key={key} >{item}</li>)

    render(){
        return(
            <div className="card">
                <div className="image">
                    <img src={this.props.details.image} alt={this.props.details.nom}/>
                </div>
                <div className="receip">
                    <h2>
                        {this.props.details.nom}
                    </h2>
                    <ul className="list-ingredients" >
                        {this.ingredients}
                    </ul>
                    <ol className="list-instructions">
                        {this.instructions}
                    </ol>
                </div>
            </div>
        )
    }

    static propTypes = {
        details: PropTypes.object.isRequired
    }

}

export default Card