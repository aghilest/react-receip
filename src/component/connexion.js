import React from "react";

class Connexion extends React.Component {

    goToReceip = e => {
        e.preventDefault()
        const pseudo = this.pseudo.value
        this.props.history.push(`/box/${pseudo}`)
    }

    render() {
        return (
            <div className="container" >
                <form className="form-signin" onSubmit={this.goToReceip}
                    id="login">
                    <h3 className="form-signin-heading">Ma Boite à Recette</h3>
                    <input type="text" className="form-control"
                        ref={input => { this.pseudo = input }}
                        id="loginEmail"
                        placeholder="Nom du Chef"
                        required autoFocus />
                    <button className="btn btn-lg btn-primary btn-block" type="submit">GO</button>
                    <p className="form-signin-heading">Pas de caractères spéciaux</p>
                </form>
            </div>
        )
    }
}

export default Connexion;