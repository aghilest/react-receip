import React from "react";

const NotFound = () => {

    return (
        <div className="not-found" >
            <div>
                <h1>
                    Page not found !
                </h1>
            </div><br />
            <a href="/" className="btn btn-primary">Home</a>
        </div>
    
        

    )
}

export default NotFound