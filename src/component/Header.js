import React from "react";
import PropTypes from "prop-types";

class Header extends React.Component {
    
    renderHeader = (pseudo) => {
        return 'aeéouiAEOUI'.indexOf(pseudo[0]) !== -1 ? `d'${pseudo}`: `de ${pseudo}`
    }

    render(){
        return(
            <header>
                <h1> La boite à recette {this.renderHeader(this.props.pseudo)} </h1>
            </header>
        )
    }

    static propTypes = {
        pseudo: PropTypes.string.isRequired
    }
} 

export default Header