//Natvie modules
import React, { Component } from 'react';
import PropTypes from "prop-types";
//Component
import Header from "./Header";
import Admin from "./Admin";
import Card from "./card";
//Data
import receips from "../receips";
//Firebase
import base from "../base";
//CSS
import "../index.css"

class App extends Component {

  state = {
    receips: {}
  }

  componentWillMount() {
    this.ref = base.syncState(
      `${this.props.match.params.pseudo}/receips`, {
        context: this,
        state: "receips"
      })
  }

  componentWillUnmount() {
    base.removeBinding(this.ref)
  }

  deleteReceip = key => {
    const receips = { ...this.state.receips }
    receips[key] = null;
    this.setState({ receips })
  }

  updateReceip = (key, updatedReceip) => {
    const receips = { ...this.state.receips }
    receips[key] = updatedReceip
    this.setState({ receips })
  }

  addReceip = receip => {
    const receips = { ...this.state.receips }
    const unicKey = Date.now();

    receips[`receip - ${ unicKey } `] = receip
    this.setState({ receips })
  }

  loadReceips = () => {
    this.setState({ receips })
  }

  render() {

    const cards = Object
      .keys(this.state.receips)
      .map(key => {
        return <Card className="card"
          key={ key } details={ this.state.receips[key] } />
      })
    return (
      <div className="box">
        <Header pseudo={ this.props.match.params.pseudo } />
        <div className="cards">
          { cards }
        </div>
        <Admin
          receips={ this.state.receips }
          loadReceips={ this.loadReceips }
          addReceip={ this.addReceip }
          updateReceip={ this.updateReceip }
          deleteReceip={ this.deleteReceip }
          pseudo={ this.props.match.params.pseudo }
        />
      </div>
    );
  }

  static propTypes = {
    match: PropTypes.object.isRequired
  }
}

export default App;