import React from "react";
import PropTypes from "prop-types";

class Formulaire extends React.Component {

    Receip = e => {
        e.preventDefault()

        const receip = {
            nom: this.receipName.value,
            image: this.receipImage.value,
            ingredients: this.receipIngredients.value,
            instructions: this.receipPreparation.value
        }
        console.log('called from Formulaire.js')
        this.props.addReceip(receip);
        this.form.reset()
    }

    render() {
        return (
            <form onSubmit={this.Receip} ref={input => this.form = input} >
                <h1>Add Receip</h1>

                <div className="required-field-block">
                    <input type="text" placeholder="Name"
                        className="form-control" required
                        ref={input => { this.receipName = input }}
                    />
                    <div className="required-icon">
                        <div className="text">*</div>
                    </div>
                </div>

                <div className="required-field-block">
                    <input type="text" placeholder="image link"
                        className="form-control" required
                        ref={input => { this.receipImage = input }}
                    />
                    <div className="required-icon">
                        <div className="text">*</div>
                    </div>
                </div>

                <div className="required-field-block">
                    <textarea rows="3" className="form-control"
                        placeholder="Ingredients..." required
                        ref={input => { this.receipIngredients = input }}
                    ></textarea>
                    <div className="required-icon">
                        <div className="text">*</div>
                    </div>
                </div>

                <div className="required-field-block">
                    <textarea rows="3" className="form-control"
                        placeholder="How to prepare ?" required
                        ref={input => { this.receipPreparation = input }}
                    ></textarea>
                    <div className="required-icon">
                        <div className="text">*</div>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary">Add</button>
            </form>
        )
    }

    static propTypes = {
        addReceip: PropTypes.func.isRequired
    }

}

export default Formulaire;