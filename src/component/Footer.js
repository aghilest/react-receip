import React from "react";
import PropTypes from "prop-types";

class Footer extends React.Component {

    render() {
        return (
            /*<footer>
                

                <button
                    onClick={this.props.logout}
                    className="btn btn-primary">
                    Déconnexion
                </button>
            </footer>*/

            <footer class="page-footer font-small unique-color-dark pt-4">
                <div class="container">
                    <ul class="list-unstyled list-inline text-center py-2">
                        <li class="list-inline-item">
                            <button
                                onClick={this.props.loadReceips}
                                className="btn btn-primary">
                                Load receip example
                            </button>
                        </li>
                        <li class="list-inline-item">
                            <button
                                onClick={this.props.logout}
                                className="btn btn-primary">
                                Déconnexion
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="footer-copyright text-center py-3">© 2018 Copyright:
                    <a href="https://mdbootstrap.com/bootstrap-tutorial/"> MDBootstrap.com</a>
                </div>

            </footer>


        )
    }

    static propTypes = {
        loadReceips: PropTypes.func.isRequired,
        logout: PropTypes.func.isRequired
    }

}

export default Footer