import React from "react";
import PropTypes from "prop-types";
import base from "../base";
import firebase from "firebase";
//component
import Formulaire from "./Formulaire";
import Footer from "./Footer";

class Admin extends React.Component {


    state = {
        uid: null,
        owner: null
    }

    componentDidMount() {
        console.log('monter')
        base.initializedApp.auth().onAuthStateChanged(user => {
            if (user) {
                this.login({ user }, null);
            }
        })
    }

    componentWillUnmount() {
        console.log('demonter')
        base.removeBinding(base.initializedApp.database)
    }

    treatChange = (e, key) => {
        const receip = this.props.receips[key]
        const upReceip = {
            ...receip,
            [e.target.name]: [e.target.value]
        }

        this.props.updateReceip(key, upReceip)
    }

    login = (result, err) => {
        if (err) {
            console.log(err)
            return;
        }

        console.log(result)

        const boxRef = firebase.database().ref(this.props.pseudo)
        boxRef.once('value', snapshot => {
            const data = snapshot.val() || {}

            if (!data.owner) {
                boxRef.set({
                    owner: result.user.uid
                })
            }

            this.setState({
                uid: result.user.uid,
                owner: data.owner || result.user.uid
            })
        })
    }

    logout = () => {
        base.initializedApp.auth().signOut().then(() => {
            console.log('signout')
            this.setState({
                uid: null
            })
        })
    }

    connexion = provider => {
        console.log('Connecting')
        if (provider === 'facebook') {
            const provider = new firebase.auth.FacebookAuthProvider()
            base.initializedApp.auth().signInWithPopup(provider)
                .then((result, err) => this.login(result, err));
        }
    }

    updateRender = key => {
        const receip = this.props.receips[key]

        return (
            <div key={key} >
                <form key={key} >
                    <h1>Update Receip {receip.nom}</h1>

                    <div className="required-field-block">
                        <input type="text" placeholder="Name"
                            name="nom"
                            className="form-control" required
                            value={receip.nom}
                            onChange={e => this.treatChange(e, key)}
                        />
                        <div className="required-icon">
                            <div className="text">*</div>
                        </div>
                    </div>

                    <div className="required-field-block">
                        <input type="text" placeholder="image link"
                            name="image"
                            className="form-control" required
                            value={receip.image}
                            onChange={e => this.treatChange(e, key)}
                        />
                        <div className="required-icon">
                            <div className="text">*</div>
                        </div>
                    </div>

                    <div className="required-field-block">
                        <textarea rows="3" className="form-control"
                            name="ingredients"
                            placeholder="Ingredients..." required
                            value={receip.ingredients}
                            onChange={e => this.treatChange(e, key)}
                        ></textarea>
                        <div className="required-icon">
                            <div className="text">*</div>
                        </div>
                    </div>

                    <div className="required-field-block">
                        <textarea className="form-control"
                            name="instructions"
                            placeholder="How to prepare ?" required
                            value={receip.instructions}
                            onChange={e => this.treatChange(e, key)}
                            rows="7"
                        ></textarea>
                        <div className="required-icon">
                            <div className="text">*</div>
                        </div>
                    </div>
                    <button
                        onClick={() => this.props.deleteReceip(key)}
                        className="btn btn-primary">
                        Delete
                    </button>
                </form>
            </div>
        )
    }

    renderLogin = () => {
        return (
            <div className="login">
                <h2>Connecte toi pour créer tes recettes</h2>
                <button className="btn btn-primary"
                    onClick={() => this.connexion('facebook')}
                >Me connecter avec Facebook</button>
            </div>
        )
    }

    render() {

        if (!this.state.uid) {
            console.log("i'm in uid test")
            console.log(this.state.uid)
            return (
                <div>
                    {this.renderLogin()}
                </div>
            )
        }

        if (this.state.uid !== this.state.owner) {
            console.log("i'm in owner test")
            console.log(`uid: ${this.state.uid} owner ${this.state.owner} `)
            return (
                <div className="login" >
                    {this.renderLogin()}
                    <p>Tu n'est pas le propriétaire de cette boite à recette !</p>
                </div>
            )
        }

        const adminCards = Object.keys(this.props.receips)
            .map(this.updateRender)

        return (
            <div>
                <Formulaire addReceip={this.props.addReceip} />
                {adminCards}
                <Footer loadReceips={this.props.loadReceips}
                        logout={this.logout}
                />
            </div>
        )
    }

    static propTypes = {
        receips: PropTypes.object.isRequired,
        loadReceips: PropTypes.func.isRequired,
        updateReceip: PropTypes.func.isRequired,
        addReceip: PropTypes.func.isRequired,
        pseudo: PropTypes.string.isRequired
    }

}

export default Admin