import Rebase from "re-base";
import firebase from "firebase";

const config = {
    apiKey: "AIzaSyA_EdKy888PU6PkEcPBGWfxk0JFTv3_0M8",
    authDomain: "receip-box.firebaseapp.com",
    databaseURL: "https://receip-box.firebaseio.com",
    projectId: "receip-box",
    storageBucket: "receip-box.appspot.com",
    messagingSenderId: "145889495638"
} 

const app = firebase.initializeApp(config)

const base = Rebase.createClass(app.database())
    
export default base;